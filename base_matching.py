import matching_main
import os
import pandas as pd
from pyjarowinkler.distance import get_jaro_distance as jarowinkler_distance
#os.chdir('/Users/joe.odonnell/Desktop/git/entity_matching')


def get_dfs(ads_path, MONTH):
    df_adobe = pd.read_csv(os.path.join(ads_path, 'adobe_' + MONTH + '.csv'))
    df_bq = pd.read_csv(os.path.join(ads_path, 'bq_' + MONTH + '.csv'))
    df_bq.columns = df_bq.columns.str.lower().str.replace(' ', '_')
    df_adobe.columns = df_adobe.columns.str.lower().str.replace(' ', '_')
    return df_adobe, df_bq

def isna(x):
    return x != x

def get_jaro_dist_col(row):
    adobe_np = row['np_adobe']
    bq_np = row['np_bq']
    if (isna(adobe_np) or isna(bq_np)):
        return 0
    else:
        return jarowinkler_distance(adobe_np, bq_np)
    
def main(ADS_PATH, NP_DIST_LIMIT, MONTH):
    adobe_df, bq_df = get_dfs(ADS_PATH, MONTH)
    adobe_df, bq_df = matching_main.filter_dfs_for_tdr(adobe_df, bq_df)
    adobe_df, bq_df = matching_main.clean_date_np_dlr_cols(adobe_df, bq_df)
    
    adobe_df = adobe_df.loc[:, ['visitor_id', 'dealer_id_adobe', 'np_adobe', 'date_adobe', 'last_touch_channel']] #there are 15 cases of NAN np
    bq_df = bq_df.loc[:, ['cupid_id', 'dealer_id_bq', 'np_bq', 'date_bq']]
    
    adobe_ids_start = adobe_df['visitor_id'].nunique()
    #bq_ids_start = bq_df['cupid_id'].nunique()
    
    adobe_bq_perf_match = adobe_df.merge(bq_df, left_on = ['dealer_id_adobe', 'date_adobe'],
                   right_on = ['dealer_id_bq', 'date_bq'],
                   how = 'inner')
    
    adobe_bq_perf_match['np_distance'] = adobe_bq_perf_match.apply(get_jaro_dist_col, axis = 1)
    adobe_bq_perf_match = adobe_bq_perf_match[adobe_bq_perf_match['np_distance'] > NP_DIST_LIMIT]
    
    adobe_bq_perf_match = adobe_bq_perf_match.sort_values('cupid_id')
    
    unique_adobe_ids_end = adobe_bq_perf_match['visitor_id'].nunique()
    #unique_bq_ids_end = adobe_bq_perf_match['cupid_id'].nunique()
    
    match_rate_adobe = unique_adobe_ids_end / adobe_ids_start
    #match_rate_bq = unique_bq_ids_end / bq_ids_start
    return adobe_bq_perf_match, match_rate_adobe
    
if __name__ == '__main__':
    ADS_PATH = '/Volumes/ADS/Audience and Insights/Data/entity_matching'
    NP_DIST_LIMIT = 0.8
    MONTH = 'july'
    
    adobe_bq_july, match_rate_july = main(ADS_PATH, NP_DIST_LIMIT, MONTH)
    
    MONTH = 'june'
    adobe_bq_june, match_rate_june = main(ADS_PATH, NP_DIST_LIMIT, MONTH)
    

    
    

