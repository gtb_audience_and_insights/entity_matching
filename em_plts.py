import pandas as pd
import seaborn as sns
import os
import matplotlib.pyplot as plt

sns.set()
ADS_PATH = '/Volumes/ADS/Audience and Insights/Data/entity_matching'
df = pd.read_csv(os.path.join(ADS_PATH, 'v_good_matches.csv'))
last_touch = df['last_touch_channel'].value_counts().reset_index()
last_touch.columns = ['Channel', 'TDRs completed']

ax = sns.barplot(x=last_touch['Channel'], y=last_touch['TDRs completed'], palette="vlag")
ax.set_xticklabels(ax.get_xticklabels(), rotation=45)
ax.set_title('Where do the test drives come from')