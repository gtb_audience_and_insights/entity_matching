import splink
from pyspark.context import SparkContext, SparkConf
from pyspark.sql import SparkSession
import pandas as pd
from pathlib import Path

def get_paths():
    adobe_path = Path('adobe_matching_data_090720.csv')
    bq_path = Path('results-20200715-141404.csv')
    return adobe_path, bq_path

# conf = SparkConf().set("spark.jars", "PATHTOJAR")
# sc = SparkContext(conf=conf)

# import os

def get_spark():
    conf=SparkConf()

    conf.set('spark.jars', 'jars/scala-udf-similarity-0.0.6.jar')
    conf.set('spark.jars.packages', 'graphframes:graphframes:0.6.0-spark2.3-s_2.11')


    conf.set("spark.sql.shuffle.partitions", "8")

    sc = SparkContext.getOrCreate(conf=conf)
    sc.setCheckpointDir("temp_graphframes/")
    spark = SparkSession(sc)

    # from pyspark.sql import types
    # spark.udf.registerJavaFunction('jaro_winkler_sim', 'uk.gov.moj.dash.linkage.JaroWinklerSimilarity', types.DoubleType())
    # spark.udf.registerJavaFunction('Dmetaphone', 'uk.gov.moj.dash.linkage.DoubleMetaphone', types.StringType())
    return spark

adobe_path, bq_path = get_paths()

spark = get_spark()


df = spark.read.csv(str(adobe_path))

spark.driver.extraClassPath /anaconda3/pkgs/pyspark-3.0.0-py_0/site-packages/pyspark/jars
