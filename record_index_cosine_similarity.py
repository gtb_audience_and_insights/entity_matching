# %%
import pandas as pd
from pathlib import Path
import os
from datetime import datetime
from tqdm import tqdm
from Levenshtein import distance as levenshtein_distance
import numpy as np
import pickle
import recordlinkage as rl
from recordlinkage.preprocessing import clean
from recordlinkage.index import Full
from recordlinkage.compare import String, Date
import math
import re
from collections import Counter

os.chdir('/Users/janell.chao/OneDrive - WPP Cloud/Code/17_Entity_Match/Input')

def get_paths():
    adobe_path = Path('adobe_matching_data_090720.csv')
    bq_path = Path('results-20200715-141404.csv')
    return adobe_path, bq_path

def get_dfs_from_paths(paths):
    adobe_path, bq_path = paths
    bq_df = pd.read_csv(bq_path)
    adobe_df = pd.read_csv(adobe_path)
    bq_df.columns = bq_df.columns.str.lower().str.replace(' ', '_')
    adobe_df.columns = adobe_df.columns.str.lower().str.replace(' ', '_')
    return adobe_df, bq_df

def get_dfs():
    paths = get_paths()
    adobe_df, bq_df = get_dfs_from_paths(paths)
    return adobe_df, bq_df

def clean_datetime_cols(adobe_df, bq_df):
    adobe_df['hour'] = adobe_df['hour'].apply(lambda x: datetime.strptime(x, '%B %d, %Y, Hour %H'))
    bq_df['dtime'] = bq_df['dtime'].apply(lambda x: datetime.strptime(x, '%d-%m-%Y-%H'))
    adobe_df.rename(columns = {'hour' : 'date_adobe'}, inplace = True)
    bq_df.rename(columns = {'dtime' : 'date_bq'}, inplace = True)
    return adobe_df, bq_df

def clean_np_cols(adobe_df, bq_df):
    adobe_df['pages'] = adobe_df['pages'].astype(str).apply(lambda x: x.lower() if 'ford' in x.lower() else np.nan)
    adobe_df['pages'] = adobe_df['pages'].str.split('ford').str.get(-1).str.replace(' ', '')
    bq_df['model_desc'] = bq_df['model_desc'].str.lower().str.replace('cx727', 'mustang')
    adobe_df.rename(columns = {'pages' : 'np_adobe'}, inplace = True)
    bq_df.rename(columns = {'model_desc' : 'np_bq'}, inplace = True)
    return adobe_df, bq_df

def clean_dlr_cols(adobe_df, bq_df):
    adobe_df.rename(columns = {'dealer_id_(p1)_(prop1)' : 'dealer_id_adobe'}, inplace = True)
    bq_df.rename(columns = {'dealer_code' : 'dealer_id_bq'}, inplace = True)
    adobe_df['dealer_id_adobe'] = adobe_df['dealer_id_adobe'].str.upper()
    return adobe_df, bq_df

def clean_date_np_dlr_cols(adobe_df, bq_df):
    adobe_df, bq_df = clean_datetime_cols(adobe_df, bq_df)
    adobe_df, bq_df = clean_np_cols(adobe_df, bq_df)
    adobe_df, bq_df = clean_dlr_cols(adobe_df, bq_df)
    return adobe_df, bq_df

def isna(x):
    return not x == x

def combine_str(df, dealer_col, np_col, date_col):
    temp = df[dealer_col].map(str) + " " + df[np_col] + " " + df[date_col].astype(str)
    return temp

def get_cosine(vec1, vec2):
    intersection = set(vec1.keys()) & set(vec2.keys())
    numerator = sum([vec1[x] * vec2[x] for x in intersection])
    sum1 = sum([vec1[x] ** 2 for x in list(vec1.keys())])
    sum2 = sum([vec2[x] ** 2 for x in list(vec2.keys())])
    denominator = math.sqrt(sum1) * math.sqrt(sum2)
    if not denominator:
        return 0.0
    else:
        return float(numerator)/denominator

def text_to_vector(text):
    WORD = re.compile(r"\w+")
    words = WORD.findall(text)
    return Counter(words)

adobe_df, bq_df = get_dfs()
adobe_df, bq_df = clean_date_np_dlr_cols(adobe_df, bq_df)
 
# market
bq_df['market'].value_counts()
adobe_df['geosegmentation_countries'].value_counts()
adobe_df = adobe_df.loc[adobe_df['geosegmentation_countries']=='united kingdom']

adobe_df = adobe_df.loc[:, ['visitor_id', 'dealer_id_adobe', 'np_adobe', 'date_adobe']] #can we do campaign code?
bq_df = bq_df.loc[:, ['cupid_id', 'dealer_id_bq', 'np_bq', 'date_bq']]

# %%
# Record Linkage
# Clean
adobe_df['np_adobe'] = clean(adobe_df['np_adobe'])
bq_df['np_bq'] = clean(bq_df['np_bq'])

# %%
# what rules to use to de-dup id?
adobe_df.drop_duplicates(['visitor_id'], inplace=True)
bq_df.drop_duplicates(['cupid_id'], inplace=True)

adobe_df.set_index(['visitor_id'], inplace = True)
bq_df.set_index(['cupid_id'], inplace = True)

# Indexing
indexer = rl.Index()
indexer.full()
pairs = indexer.index(adobe_df, bq_df)
print (len(adobe_df), len(bq_df), len(pairs))

# Comparison
c = rl.Compare()
c.string('dealer_id_adobe', 'dealer_id_bq', label='dealer')
c.string('np_adobe', 'np_bq', label='np')
# compare attributes of pairs with date algorithm
c.date('date_adobe', 'date_bq', label='date')

feature_vectors = c.compute(pairs, adobe_df, bq_df)

# Threshold-based: 2
# how do we decide on thresholds?
feature_vectors.sum(axis=1).value_counts().sort_index(ascending=False)
predictions = feature_vectors[feature_vectors.sum(axis=1) > 2].reset_index()
print("Threshold-Based: {} matches".format(len(predictions)))

feature_vectors['recordlinkage_avg'] = feature_vectors.mean(axis = 1)
feature_vectors.reset_index(inplace=True)

feature_vectors = feature_vectors.merge(adobe_df, on='visitor_id', how='left')
feature_vectors = feature_vectors.merge(bq_df, on='cupid_id', how='left')

# %%
# Cosine similarity
# combine str
feature_vectors['adobe'] = combine_str(feature_vectors, 'dealer_id_adobe', 'np_adobe', 'date_adobe').fillna("")
feature_vectors['bq'] = combine_str(feature_vectors, 'dealer_id_bq', 'np_bq', 'date_bq').fillna("")

# compute cosine similarity
vector_adobe = feature_vectors["adobe"].apply(lambda s: text_to_vector(s)) 
vector_bq = feature_vectors["bq"].apply(lambda s: text_to_vector(s))

feature_vectors['cosine_sim'] = [get_cosine(vector_adobe[i], vector_bq[i]) for i in range(len(feature_vectors))]

feature_vectors = feature_vectors.drop(["adobe", "bq"], axis=1)

# test
feature_vectors.loc[feature_vectors['cosine_sim'] == feature_vectors['cosine_sim'].max()]
feature_vectors.loc[feature_vectors['cosine_sim'] == feature_vectors['cosine_sim'].min()]

feature_vectors.to_csv("feature_vectors.csv")

