import pandas as pd
from pathlib import Path
import os
from datetime import datetime, timedelta
from tqdm import tqdm
# from Levenshtein import distance as levenshtein_distance
import numpy as np
import pickle
from pyjarowinkler import distance as jarowinkler_distance
#os.chdir('/Users/joe.odonnell/Desktop/jags_things/entity_matching')

def get_paths():
    adobe_path = Path('adobe_matching_data_090720.csv')
    bq_path = Path('results-20200715-141404.csv')
    return adobe_path, bq_path

def get_dfs_from_paths(paths):
    adobe_path, bq_path = paths
    bq_df = pd.read_csv(bq_path)
    adobe_df = pd.read_csv(adobe_path)
    bq_df.columns = bq_df.columns.str.lower().str.replace(' ', '_')
    adobe_df.columns = adobe_df.columns.str.lower().str.replace(' ', '_')
    return adobe_df, bq_df

def filter_adobe_for_uk(df):
    df = df[df['geosegmentation_countries'] == 'united kingdom']
    return df

def get_dfs():
    paths = get_paths()
    adobe_df, bq_df = get_dfs_from_paths(paths)
    return adobe_df, bq_df

def clean_datetime_cols(adobe_df, bq_df):
    adobe_df['hour'] = adobe_df['hour'].apply(lambda x: datetime.strptime(x, '%B %d, %Y, Hour %H'))
    bq_df['dtime'] = bq_df['dtime'].apply(lambda x: datetime.strptime(x, '%d-%m-%Y-%H'))
    adobe_df.rename(columns = {'hour' : 'date_adobe'}, inplace = True)
    bq_df.rename(columns = {'dtime' : 'date_bq'}, inplace = True)
    return adobe_df, bq_df

def clean_np_cols(adobe_df, bq_df):
    adobe_df['pages'] = adobe_df['pages'].astype(str).apply(lambda x: x.lower() if 'ford' in x.lower() else np.nan)
    adobe_df['pages'] = adobe_df['pages'].str.split('ford').str.get(-1).str.replace(' ', '')
    bq_df['model_desc'] = bq_df['model_desc'].str.lower().str.replace('cx727', 'mustang')
    adobe_df.rename(columns = {'pages' : 'np_adobe'}, inplace = True)
    bq_df.rename(columns = {'model_desc' : 'np_bq'}, inplace = True)
    return adobe_df, bq_df

def clean_dlr_cols(adobe_df, bq_df):
    adobe_df.rename(columns = {'dealer_id_(p1)_(prop1)' : 'dealer_id_adobe'}, inplace = True)
    bq_df.rename(columns = {'dealer_code' : 'dealer_id_bq'}, inplace = True)
    bq_df['dealer_id_bq'] = bq_df['dealer_id_bq'].astype(str)
    adobe_df['dealer_id_adobe'] = adobe_df['dealer_id_adobe'].str.upper().astype(str)
    adobe_df['dealer_id_adobe'] = adobe_df['dealer_id_adobe'].apply(lambda x: x[2:] if x[:2] == 'UB' else x)
    return adobe_df, bq_df

def clean_date_np_dlr_cols(adobe_df, bq_df):
    adobe_df = filter_adobe_for_uk(adobe_df)
    adobe_df, bq_df = clean_datetime_cols(adobe_df, bq_df)
    adobe_df, bq_df = clean_np_cols(adobe_df, bq_df)
    adobe_df, bq_df = clean_dlr_cols(adobe_df, bq_df)
    return adobe_df, bq_df

def isna(x):
    return not x == x

def define_date_np_dlr_bq(row):
    date_bq = row['date_bq']
    np_bq = row['np_bq']
    dlr_bq = row['dealer_id_bq']
    return date_bq, np_bq, dlr_bq    

def add_key_to_dicts(index_bq, bq_adobe_date_dict, bq_adobe_np_dict, bq_adobe_dlr_dict):
    bq_adobe_date_dict[index_bq] = {}
    bq_adobe_np_dict[index_bq] = {}
    bq_adobe_dlr_dict[index_bq] = {}
    return bq_adobe_date_dict, bq_adobe_np_dict, bq_adobe_dlr_dict

def define_date_np_dlr_adobe(row):
    adobe_id = row['visitor_id']
    dlr_adobe = row['dealer_id_adobe']
    np_adobe = row['np_adobe']
    date_adobe = row['date_adobe']
    return adobe_id, dlr_adobe, np_adobe, date_adobe 

def add_time_diff(bq_adobe_date_dict, date_bq, date_adobe, index_bq, adobe_id):
    date_diff = abs(date_bq - date_adobe)
    total_seconds_diff = int(date_diff.total_seconds())
    bq_adobe_date_dict[index_bq][adobe_id] = total_seconds_diff
    return bq_adobe_date_dict

def add_np_distance(bq_adobe_np_dict, np_bq, np_adobe, index_bq, adobe_id):
    if isna(np_bq) or isna(np_adobe):
        lev_distance = 0 #magic number, should change maybe smaller?
    else:
        lev_distance = jarowinkler_distance.get_jaro_distance(np_bq, np_adobe) #levenshtein distance not great cos 'ford focus' and 'focus' is the same as 'ka' and 'focus'
    bq_adobe_np_dict[index_bq][adobe_id] = lev_distance
    return bq_adobe_np_dict

def add_dlr_distance(bq_adobe_dlr_dict, dlr_bq, dlr_adobe, index_bq, adobe_id):
    if isna(dlr_bq) or isna(dlr_adobe):
        dlr_distance = 0
    else:
        dlr_distance = int(dlr_adobe == dlr_bq)
    bq_adobe_dlr_dict[index_bq][adobe_id] = dlr_distance #overwrites adobe_id which isn't great
    return bq_adobe_dlr_dict

def get_dicts_of_match_results(bq_df, adobe_df):
    bq_adobe_date_dict = {}
    bq_adobe_np_dict = {}
    bq_adobe_dlr_dict = {}
    for index_bq, row in bq_df.loc[:, ['cupid_id', 'date_bq', 'np_bq', 'dealer_id_bq']].iterrows():
        date_bq, np_bq, dlr_bq = define_date_np_dlr_bq(row)
        bq_adobe_date_dict, bq_adobe_np_dict, bq_adobe_dlr_dict =\
            add_key_to_dicts(index_bq, bq_adobe_date_dict, bq_adobe_np_dict, bq_adobe_dlr_dict)
        for index_ad, row in adobe_df.loc[:, ['visitor_id', 'dealer_id_adobe', 'np_adobe', 'date_adobe']].iterrows():
            adobe_id, dlr_adobe, np_adobe, date_adobe = define_date_np_dlr_adobe(row)
            bq_adobe_date_dict = add_time_diff(bq_adobe_date_dict, date_bq, date_adobe, index_bq, adobe_id)
            bq_adobe_np_dict = add_np_distance(bq_adobe_np_dict, np_bq, np_adobe, index_bq, adobe_id)
            bq_adobe_dlr_dict = add_dlr_distance(bq_adobe_dlr_dict, dlr_bq, dlr_adobe, index_bq, adobe_id)
    return bq_adobe_date_dict, bq_adobe_np_dict, bq_adobe_dlr_dict

def get_dict_of_dfs_by_day(adobe_df, bq_df):
    min_date = datetime.strptime(adobe_df['date_adobe'].min().strftime('%d%m%Y'), '%d%m%Y')
    max_date = datetime.strptime(adobe_df['date_adobe'].max().strftime('%d%m%Y'), '%d%m%Y') + timedelta(days=1)
    
    adobe_dfs = {}
    bq_dfs = {}
    delta = timedelta(days=1)
    ii = 0
    while min_date < max_date:
        min_date_mask = adobe_df['date_adobe'] >= min_date
        max_date_mask = adobe_df['date_adobe'] < min_date + delta
        adobe_temp_df = adobe_df[min_date_mask & max_date_mask]
        
        min_date_mask = bq_df['date_bq'] >= min_date
        max_date_mask = bq_df['date_bq'] < min_date + delta
        bq_temp_df = bq_df[min_date_mask & max_date_mask]
        
        adobe_dfs['day_' + str(ii)] = adobe_temp_df.copy()
        bq_dfs['day_' + str(ii)] = bq_temp_df.copy()
        ii += 1
        min_date += delta
    return adobe_dfs, bq_dfs

def get_match_results_dicts_by_day(adobe_dfs, bq_dfs):
    date_by_day_dict = {}
    np_by_day_dict = {}
    dlr_by_day_dict = {}
    for day in tqdm(adobe_dfs.keys()):
        bq_adobe_date_dict, bq_adobe_np_dict, bq_adobe_dlr_dict = get_dicts_of_match_results(bq_dfs[day], adobe_dfs[day])
        date_by_day_dict[day] = bq_adobe_date_dict
        np_by_day_dict[day] = bq_adobe_np_dict
        dlr_by_day_dict[day] = bq_adobe_dlr_dict
    return date_by_day_dict, np_by_day_dict, dlr_by_day_dict

def save_dicts(bq_adobe_date_dict, bq_adobe_np_dict, bq_adobe_dlr_dict):
    pickle.dump(bq_adobe_date_dict, open( "date_dict.p", "wb" ) )
    pickle.dump(bq_adobe_np_dict, open( "np_dict.p", "wb" ) )
    pickle.dump(bq_adobe_dlr_dict, open( "dlr_dict.p", "wb" ) )
    return None

def load_dicts():
    bq_adobe_date_dict = pickle.load( open( "date_dict.p", "rb" ) )
    bq_adobe_np_dict = pickle.load( open( "np_dict.p", "rb" ) )
    bq_adobe_dlr_dict = pickle.load( open( "dlr_dict.p", "rb" ) )
    return bq_adobe_date_dict, bq_adobe_np_dict, bq_adobe_dlr_dict

def create_results_dict(bq_adobe_date_dict, bq_adobe_np_dict, bq_adobe_dlr_dict, use_date_time = True):
    results_dict = {}
    results_dict['cupid_index'] = []
    results_dict['adobe_id'] = []
    results_dict['match_result'] = []
    results_dict['dlr_match'] = []
    results_dict['date_match'] = []
    results_dict['np_match'] = []
    if bq_adobe_date_dict:
        adobe_key = list(bq_adobe_date_dict.keys())[0]
    else:
        return {}
    for cupid_index in tqdm(bq_adobe_date_dict.keys()):
        for adobe_id in bq_adobe_date_dict[adobe_key].keys():
            date_diff = bq_adobe_date_dict.get(cupid_index)[adobe_id]
            np_lev = bq_adobe_np_dict[cupid_index][adobe_id]
            dlr_distance = bq_adobe_dlr_dict[cupid_index][adobe_id]
            match_result = averaging_funcion(date_diff, np_lev, dlr_distance, WEIGHTS, use_date_time) #higher the better
            results_dict['cupid_index'].append(cupid_index)
            results_dict['adobe_id'].append(adobe_id)
            results_dict['match_result'].append(match_result)
            results_dict['dlr_match'].append(dlr_distance)
            results_dict['date_match'].append(date_diff)
            results_dict['np_match'].append(np_lev)
    return results_dict

def get_results_by_day(date_by_day_dict, np_by_day_dict, dlr_by_day_dict, use_date_time = True):
    results_by_day = {}
    for day in date_by_day_dict.keys():
        print(day)
        results_by_day[day] = create_results_dict(date_by_day_dict[day],
                                                    np_by_day_dict[day],
                                                    dlr_by_day_dict[day],
                                                    use_date_time)
    return results_by_day

def clean_results_dict(results_dict):
    df = pd.DataFrame(results_dict)
    df = df.merge(bq_df['cupid_id'].reset_index(), how = 'inner',
                  left_on = 'cupid_index', right_on = 'index')
    
    df_temp = df.groupby(['cupid_id'])['match_result'].max().reset_index()
    df = df.merge(df_temp, how = 'inner', on = ['cupid_id', 'match_result'])
    
    df = df.merge(adobe_df, how = 'inner', left_on = 'adobe_id', right_on = 'visitor_id')
    df = df.merge(bq_df, how = 'inner', on = 'cupid_id')
    df.drop(['cupid_index', 'visitor_id'], axis = 1, inplace = True)
    return df

def get_final_results(results_by_day):
    results = {}
    for day in results_by_day.keys():
        if results_by_day[day]: 
            df_temp = clean_results_dict(results_by_day[day])
            results[day] = df_temp
        else:
            continue
    return results

def make_bq_plus_minus_hour(bq_df):
    bq_df_t_minus_1 = bq_df.copy()
    bq_df_t_plus_1 = bq_df.copy()
    bq_df_t_minus_1['date_bq'] = bq_df_t_minus_1['date_bq'] - timedelta(hours=1)
    bq_df_t_plus_1['date_bq'] += timedelta(hours = 1)
    return bq_df_t_minus_1, bq_df_t_plus_1

def get_match_numbers_df(results, match_number, match_date_type):
    match_numbers = {}
    match_numbers['day'] = []
    match_numbers['number total cupid_id'] = []
    match_numbers['number matched well cupid id'] = []
    match_numbers['match_date_type'] = []
    for day in results.keys():
        no_cupid_id = results[day]['cupid_id'].nunique()
        no_good_match = results[day][results[day]['match_result'] > match_number]['cupid_id'].nunique()
        #for making df
        match_numbers['day'].append(day)
        match_numbers['number total cupid_id'].append(no_cupid_id)
        match_numbers['number matched well cupid id'].append(no_good_match)
        match_numbers['match_date_type'].append(match_date_type)
    df = pd.DataFrame(match_numbers)
    return df

def get_good_matches_df(results, match_number, match_date_type):
    good_matches_dfs = []
    for day in results.keys():
        temp_df = results[day][results[day]['match_result'] > match_number]
        temp_df['day'] = day
        temp_df['match_date_type'] = match_date_type
        good_matches_dfs.append(temp_df)
    good_matches_df = pd.concat(good_matches_dfs, axis = 0)
    return good_matches_df

def averaging_funcion(date, np_lev, dlr_distance, WEIGHTS, use_date_time = True):
    no_days = date / 86400 #no_seconds in a day
    if use_date_time:
        day_weight = WEIGHTS['days'] * (2 - no_days) ** 3
    else:
        day_weight = 0
    np_lev_weight = WEIGHTS['np_lev'] * ((2 * np_lev)) ** 3
    if np_lev < 0.5:
        np_lev = 0
    dlr_weight = WEIGHTS['dlr'] * dlr_distance * 8
    return day_weight + np_lev_weight + dlr_weight

def filter_adobe_for_tdr(adobe_df):
    is_tdr_ser = adobe_df['pages'].str.contains('test drive')
    adobe_df['TDR'] = is_tdr_ser.replace((True,False), ('TDR',''))
    adobe_df = adobe_df[adobe_df['TDR'] == 'TDR']
    return adobe_df

def filter_cupid_for_tdr(cupid_df):
    cupid_df = cupid_df[cupid_df['treatment_type'] == 'TDR']
    return cupid_df

def filter_dfs_for_tdr(adobe_df, cupid_df):
    adobe_df = filter_adobe_for_tdr(adobe_df)
    cupid_df = filter_cupid_for_tdr(cupid_df)
    return adobe_df, cupid_df

if __name__ == '__main__':
    
    WEIGHTS = {'dlr' : 1,
               'days' : 1,
               'np_lev' : 0.2}
    
    use_date_time = True
    
    adobe_df, bq_df = get_dfs()
    adobe_df, bq_df = filter_dfs_for_tdr(adobe_df, bq_df)
    adobe_df, bq_df = clean_date_np_dlr_cols(adobe_df, bq_df)
    
    adobe_df = adobe_df.loc[:, ['visitor_id', 'dealer_id_adobe', 'np_adobe', 'date_adobe']] #can we do campaign code?
    bq_df = bq_df.loc[:, ['cupid_id', 'dealer_id_bq', 'np_bq', 'date_bq']]
    
    bq_df_t_minus_1, bq_df_t_plus_1 = make_bq_plus_minus_hour(bq_df)

    adobe_dfs, bq_dfs = get_dict_of_dfs_by_day(adobe_df, bq_df)
    _, bq_df_t_minus_1s = get_dict_of_dfs_by_day(adobe_df, bq_df_t_minus_1)
    _, bq_df_t_plus_1s = get_dict_of_dfs_by_day(adobe_df, bq_df_t_plus_1)
    
    
    date_by_day_dict, np_by_day_dict, dlr_by_day_dict = get_match_results_dicts_by_day(adobe_dfs, bq_dfs)
    results_by_day = get_results_by_day(date_by_day_dict, np_by_day_dict, dlr_by_day_dict, use_date_time) #WEIGHTS used
    use_date_time = False
    results_no_time_by_day = get_results_by_day(date_by_day_dict, np_by_day_dict, dlr_by_day_dict, use_date_time) #WEIGHTS used
    results = get_final_results(results_by_day)
    results_no_time = get_final_results(results_no_time_by_day)
    
    use_date_time = True
    date_by_day_dict_t_minus_1, np_by_day_dict_t_minus_1, dlr_by_day_dict_t_minus_1 = get_match_results_dicts_by_day(adobe_dfs, bq_df_t_minus_1s)
    results_by_day_t_minus_1 = get_results_by_day(date_by_day_dict_t_minus_1, np_by_day_dict_t_minus_1, dlr_by_day_dict_t_minus_1, use_date_time)
    results_t_minus_1 = get_final_results(results_by_day_t_minus_1)
    
    date_by_day_dict_t_plus_1, np_by_day_dict_t_plus_1, dlr_by_day_dict_t_plus_1 = get_match_results_dicts_by_day(adobe_dfs, bq_df_t_plus_1s)
    results_by_day_t_plus_1 = get_results_by_day(date_by_day_dict_t_plus_1, np_by_day_dict_t_plus_1, dlr_by_day_dict_t_plus_1, use_date_time)
    results_t_plus_1 = get_final_results(results_by_day_t_plus_1)
    
    match_date_types = ['original_date_time', 'cupid_minus_1_hour', 'cupid_plus_1_hour']
    match_numbers_dfs = []
    for ii, res in enumerate([results, results_t_minus_1, results_t_plus_1]):
        df_sum_temp = get_match_numbers_df(res, 12, match_date_types[ii])
        match_numbers_dfs.append(df_sum_temp)
        
    match_numbers_dfs.append(get_match_numbers_df(results_no_time, 6, 'no time'))
    match_numbers_df = pd.concat(match_numbers_dfs, axis = 0)
    
    df_good_matches = []
    for ii, res in enumerate([results, results_t_minus_1, results_t_plus_1]):
        df_good_matches_temp = get_good_matches_df(results, 12, match_date_types[0])
        df_good_matches.append(df_good_matches_temp)
        
    df_good_matches.append(get_good_matches_df(results_no_time, 6, 'no time'))
        
    df_good_matches = pd.concat(df_good_matches, axis = 0)
    
    df_good_matches.to_csv('tdr_goodish_matches.csv')




