import pandas as pd
import os
from datetime import datetime, timedelta
from tqdm import tqdm
import numpy as np
import pickle
import py_entitymatching as em

def get_paths():
    adobe_path = 'adobe_matching_data_090720.csv'
    bq_path = 'results-20200715-141404.csv'
    return adobe_path, bq_path
def get_dfs_from_paths(paths):
    adobe_path, bq_path = paths
    bq_df = em.read_csv_metadata(bq_path)
    adobe_df = em.read_csv_metadata(adobe_path)
    bq_df.columns = bq_df.columns.str.lower().str.replace(' ', '_')
    adobe_df.columns = adobe_df.columns.str.lower().str.replace(' ', '_')
    return adobe_df, bq_df
def filter_adobe_for_uk(df):
    df = df[df['geosegmentation_countries'] == 'united kingdom']
    return df
def get_dfs():
    paths = get_paths()
    adobe_df, bq_df = get_dfs_from_paths(paths)
    return adobe_df, bq_df
def clean_datetime_cols(adobe_df, bq_df):
    adobe_df['hour'] = adobe_df['hour'].apply(lambda x: datetime.strptime(x, '%B %d, %Y, Hour %H'))
    bq_df['dtime'] = bq_df['dtime'].apply(lambda x: datetime.strptime(x, '%d-%m-%Y-%H'))
    adobe_df.rename(columns = {'hour' : 'date_adobe'}, inplace = True)
    bq_df.rename(columns = {'dtime' : 'date_bq'}, inplace = True)
    return adobe_df, bq_df
def clean_np_cols(adobe_df, bq_df):
    adobe_df['pages'] = adobe_df['pages'].astype(str).apply(lambda x: x.lower() if 'ford' in x.lower() else np.nan)
    adobe_df['pages'] = adobe_df['pages'].str.split('ford').str.get(-1).str.replace(' ', '')
    bq_df['model_desc'] = bq_df['model_desc'].str.lower().str.replace('cx727', 'mustang')
    adobe_df.rename(columns = {'pages' : 'np_adobe'}, inplace = True)
    bq_df.rename(columns = {'model_desc' : 'np_bq'}, inplace = True)
    return adobe_df, bq_df
def clean_dlr_cols(adobe_df, bq_df):
    adobe_df.rename(columns = {'dealer_id_(p1)_(prop1)' : 'dealer_id_adobe'}, inplace = True)
    bq_df.rename(columns = {'dealer_code' : 'dealer_id_bq'}, inplace = True)
    bq_df['dealer_id_bq'] = bq_df['dealer_id_bq'].astype(str)
    adobe_df['dealer_id_adobe'] = adobe_df['dealer_id_adobe'].str.upper().astype(str)
    adobe_df['dealer_id_adobe'] = adobe_df['dealer_id_adobe'].apply(lambda x: x[2:] if x[:2] == 'UB' else x)
    return adobe_df, bq_df
def clean_date_np_dlr_cols(adobe_df, bq_df):
    adobe_df = filter_adobe_for_uk(adobe_df)
    adobe_df, bq_df = clean_datetime_cols(adobe_df, bq_df)
    adobe_df, bq_df = clean_np_cols(adobe_df, bq_df)
    adobe_df, bq_df = clean_dlr_cols(adobe_df, bq_df)
    return adobe_df, bq_df
def isna(x):
    return not x == x
def filter_adobe_for_tdr(adobe_df):
    is_tdr_ser = adobe_df['pages'].str.contains('test drive')
    adobe_df['TDR'] = is_tdr_ser.replace((True,False), ('TDR',''))
    adobe_df = adobe_df[adobe_df['TDR'] == 'TDR']
    return adobe_df

def filter_cupid_for_tdr(cupid_df):
    cupid_df = cupid_df[cupid_df['treatment_type'] == 'TDR']
    return cupid_df

def filter_dfs_for_tdr(adobe_df, cupid_df):
    adobe_df = filter_adobe_for_tdr(adobe_df)
    cupid_df = filter_cupid_for_tdr(cupid_df)
    return adobe_df, cupid_df

def rename_corresponding_cols(adobe_df, bq_df):
    bq_df.rename(columns = {'dealer_id_bq' : 'dealer_id',
                            'np_bq' : 'np',
                            'date_bq' : 'date'}, inplace = True)
    adobe_df.rename(columns = {'dealer_id_adobe' : 'dealer_id',
                               'np_adobe' : 'np',
                               'date_adobe' : 'date'}, inplace = True)
    return adobe_df, bq_df

if __name__ == '__main__':
    adobe_df, bq_df = get_dfs()
    adobe_df, bq_df = filter_dfs_for_tdr(adobe_df, bq_df)
    adobe_df, bq_df = clean_date_np_dlr_cols(adobe_df, bq_df)
    adobe_df = adobe_df.loc[:, ['visitor_id', 'dealer_id_adobe', 'np_adobe', 'date_adobe']]
    adobe_df = adobe_df.reset_index()
    adobe_df.rename(columns = {'index' : 'new_adobe_id'}, inplace = True)
    bq_df = bq_df.reset_index()
    bq_df.rename(columns = {'index' : 'new_cupid_id'}, inplace = True)
    adobe_df, bq_df = rename_corresponding_cols(adobe_df, bq_df)
    
    em.set_key(adobe_df, 'new_adobe_id')
    em.set_key(bq_df, 'new_cupid_id')
    
    adobe_df.to_csv('em_adobe_df.csv', index = False)
    bq_df.to_csv('em_bq_df.csv', index = False)
    
    adobe_df = em.read_csv_metadata('em_adobe_df.csv', key='new_adobe_id')
    bq_df = em.read_csv_metadata('em_bq_df.csv', key='new_cupid_id')
    

    
    atypes_bq = em.get_attr_types(bq_df)
    atypes_adobe = em.get_attr_types(adobe_df)
    
    block_c = em.get_attr_corres(bq_df, adobe_df)
    
    
    ob = em.OverlapBlocker()
    C = ob.block_tables(adobe_df, bq_df, 'dealer_id', 'dealer_id', 
                        l_output_attrs=['np', 'date'], 
                        r_output_attrs=['np', 'date'],
                        overlap_size=1, show_progress=False)
    
    S = em.sample_table(C, 230)
    #S.to_csv('labelled_data.csv')
    
    
    labelled_data = pd.read_csv('labelled_data.csv')

    feature_table = em.get_features_for_matching(adobe_df, bq_df)
    
    attrs_from_table = ['ltable_np', 'ltable_date',
                        'rtable_np', 'rtable_date']
    
    H = em.extract_feature_vecs(labelled_data, 
                            feature_table=feature_table, 
                            attrs_before = attrs_from_table,
                            attrs_after='label',
                            show_progress=False)

    metadata_file = 'adobe_df.metadata'
    with open(metadata_file, 'w') as the_file:
        the_file.write('#key=new_adobe_id')
    sample_A, sample_B = em.down_sample(adobe_df, bq_df, size=1000, y_param=100, show_progress=False)
